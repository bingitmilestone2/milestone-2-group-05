﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace milestone_2_group_05.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult SecHome()
        {
            ViewData["Message"] = "50% 50%.";

            return View();
        }
        public IActionResult Contact()
        {
            ViewData["Message"] = "Who We Are";

            return View();
        }

        public IActionResult Portfolio()
        {
            ViewData["Message"] = "";

            return View();
        }
        
        public IActionResult Mediagallery()
        {
            ViewData["Message"] = "";

            return View();
        }

        public IActionResult Blog ()
        {
            ViewData["Message"] = "Blog Page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
        public IActionResult Profile()
        {
            return View();
        }
    }
}
