# READ THIS!!!

This project use dotnet core version 1.0.1
Install the correct runtime first by [clicking here](https://download.microsoft.com/download/6/F/B/6FB4F9D2-699B-4A40-A674-B7FF41E0E4D2/dotnet-win-x64.1.1.4.exe)

After you have installed it check that you are running the correct version by typing this into git bash:

`dotnet --version`